import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import { TextInput, Button } from 'react-native-paper';
import { Picker } from 'native-base';

export default function HomeScreen(props) {
  const [name, setName] = React.useState('');
  const [data2, setData2] = React.useState('');
  const [type, setType] = React.useState('website');

  const rememberData = async () => {
    if (name == '' || data == '' || type == '') {
      alert('Need enter some data');
      return;
    }

    const data = {
      name,
      data: data2,
      type,
    };

    await AsyncStorage.setItem(Date.now().toString(), JSON.stringify(data))
      .then(() => {
        props.navigation.goBack();
      })
      .catch((error) => console.log(error));
  };

  return (
    <View style={styles.container}>
      <TextInput
        label='Name'
        value={name}
        onChangeText={(text) => setName(text)}
      />
      <TextInput
        style={styles.input}
        label='Data'
        value={data2}
        onChangeText={(text) => setData2(text)}
      />
      <Text style={{ ...styles.input, marginLeft: 5 }}>Type</Text>
      <Picker
        placeholder='Select type'
        selectedValue={type}
        onValueChange={(text) => setType(text)}
        note
        mode='dropdown'
      >
        <Picker.Item label='Links' value='links' />
        <Picker.Item label='Location' value='location' />
        <Picker.Item label='Phone' value='phone' />
        <Picker.Item label='Website' value='website' />
        <Picker.Item label='EMail' value='email' />
      </Picker>

      <Button
        mode='contained'
        style={styles.input}
        onPress={() => {
          rememberData();
        }}
      >
        Remember this
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  input: {
    marginTop: 20,
  },
});
