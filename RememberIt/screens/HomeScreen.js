import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import { AppLoading } from 'expo';
import { useFonts } from '@use-expo/font';
import { ActivityIndicator } from 'react-native-paper';

const customFonts = {
  PottaOne: require('./../assets/fonts/potta.ttf'),
};

export default function HomeScreen(props) {
  const [data2, setData2] = React.useState([]);
  const [isLoaded] = useFonts(customFonts);

  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getAllData();
    });

    return unsubscribe;
  }, []);

  const clearAllData = async () => {
    await AsyncStorage.getAllKeys()
      .then((keys) => AsyncStorage.multiRemove(keys))
      .then(() => {
        setData2([]);
        alert('Successfully cleaned');
      });
  };

  const getAllData = async () => {
    await AsyncStorage.getAllKeys()
      .then(async (keys) => {
        try {
          const data = await AsyncStorage.multiGet(keys);
          if (!data) {
            setData2([]);
          } else {
            setData2(data);
          }
        } catch (error) {
          return console.log(error);
        }
      })
      .catch((error) => console.log(error));
  };

  if (!isLoaded) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size='large' />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        style={{ width: '80%' }}
        data={data2}
        keyExtractor={(item, index) => item[0].toString()}
        renderItem={({ item }) => {
          const rememberData = JSON.parse(item[1]);

          return (
            <TouchableOpacity
              onPress={() => {
                console.log('Data::: ' + rememberData.data);

                try {
                  if (rememberData.type == 'links') {
                    Linking.openURL(rememberData.data);
                  } else if (rememberData.type == 'location') {
                    // https://www.google.com/maps/place/callifornia
                    Linking.openURL(
                      `https://www.google.com/maps/place/${rememberData.data}`
                    ).catch((error) => {
                      alert('E:' + error);
                    });
                  } else if (rememberData.type == 'phone') {
                    Linking.openURL(`tel:${rememberData.data}`).catch(
                      (error) => {
                        alert('E:' + error);
                      }
                    );
                  } else if (rememberData.type == 'website') {
                    Linking.openURL(rememberData.data).catch((error) => {
                      alert('E:' + error);
                    });
                  } else if (rememberData.type == 'email') {
                    Linking.openURL(`mailto:${rememberData.data}`).catch(
                      (error) => {
                        alert('E:' + error);
                      }
                    );
                  }
                } catch (err) {
                  alert('Linking error::: ' + err.toString());
                }
              }}
            >
              <View style={styles.listItem}>
                <Text style={styles.oneItem}>Name: {rememberData.name}</Text>
                <Text style={styles.oneItem}>Data: {rememberData.data}</Text>
                <Text style={styles.oneItem}>Type: {rememberData.type}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />

      <TouchableOpacity
        activeOpacity={0.5}
        style={styles.fab}
        onLongPress={() => {
          if (data2 && data2.length > 0) {
            clearAllData();
          }
        }}
        onPress={() => {
          props.navigation.navigate('Add');
        }}
      >
        <MaterialIcons name='add' size={20} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  fab: {
    backgroundColor: '#f1c40f',
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,

    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,

    position: 'absolute',
    right: 40,
    bottom: 40,
  },
  listItem: {
    flexDirection: 'column',
    paddingHorizontal: 10,
    paddingVertical: 20,
    marginTop: 12,
    borderRadius: 8,
    backgroundColor: '#cc33ff',
  },
  oneItem: {
    color: 'black',
    fontFamily: 'PottaOne',
    fontSize: 15,
  },
});
