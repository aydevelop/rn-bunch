import * as React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';

export default class App extends React.Component {
  state = {
    coin_result: 'head',
  };

  flip() {
    this.setState({
      coin_result: this.state.coin_result == 'head' ? 'tail' : 'head',
    });
  }

  render() {
    var img1 = (
      <Image
        source={require('./assets/coin_h.png')}
        style={{ width: 150, height: 150, marginBottom: 20, marginTop: 10 }}
      />
    );

    var img2 = (
      <Image
        source={require('./assets/coin_t.png')}
        style={{ width: 150, height: 150, marginBottom: 20, marginTop: 10 }}
      />
    );

    return (
      <View style={styles.container}>
        <Text>{this.state.coin_result.toUpperCase()}</Text>
        {this.state.coin_result == 'head' ? img1 : img2}
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={() => {
            this.flip();
          }}
        >
          <Text>[ Flip a Coin ]</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e5e7e9',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#f1c40f',
    padding: 20,
    margin: 10,
    borderRadius: 4,
  },
});
