export const getRemaining = (time) => {
  const minutes = formatNumber(Math.floor(time / 60));
  const seconds = formatNumber(time - minutes * 60);
  return { minutes, seconds };
};

export const formatNumber = (number) => {
  return `0${number}`.slice(-2);
};

export const createArray = (length) => {
  const arr = [];
  let i = 1;
  while (i <= length) {
    arr.push(i.toString());
    i += 1;
  }
  return arr;
};
