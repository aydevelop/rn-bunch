import { StatusBar } from 'expo-status-bar';
import React, { useState, useRef, useEffect } from 'react';
import { Picker } from '@react-native-picker/picker';
import RNPickerSelect from 'react-native-picker-select';
import { getRemaining, createArray } from './helper';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const AVAILABLE_MIN = createArray(16);
const AVAILABLE_SEC = createArray(60);
const { width, height } = Dimensions.get('window');

export default function App(props) {
  let intervalId = useRef(null);
  const [selectedValue, setSelectedValue] = useState(5);
  const [values, setValues] = useState({
    remainingSeconds: 15,
    isRunning: false,
  });

  const { remainingSeconds, isRunning } = values;
  const { minutes, seconds } = getRemaining(remainingSeconds);

  useEffect(() => {
    return () => {
      clearInterval(intervalId.current);
    };
  }, []);

  useEffect(() => {
    console.log('check.... ' + remainingSeconds);
    if (remainingSeconds == 0) {
      stop();
    }
  });

  const start = () => {
    console.log('start');

    setValues((prev) => {
      return {
        ...values,
        remainingSeconds: parseInt(selectedValue) * 60,
        isRunning: true,
      };
    });

    intervalId.current = setInterval(() => {
      setValues((prev) => {
        let sec = prev.remainingSeconds - 1;
        return { ...values, remainingSeconds: sec, isRunning: true };
      });
    }, 1000);
  };

  const stop = () => {
    console.log('stop');
    clearInterval(intervalId.current);
    setValues((prev) => {
      return { ...values, remainingSeconds: 15, isRunning: false };
    });
  };

  const renderPickers = () => {
    return (
      <View
        style={{
          backgroundColor: '#89AAFF',
          width: width / 3,
        }}
      >
        <Picker
          mode='dialog'
          selectedValue={selectedValue}
          style={{
            color: 'black',
          }}
          itemStyle={{}}
          onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        >
          {AVAILABLE_MIN.map((value) => (
            <Picker.Item key={value} label={value} value={value} />
          ))}
        </Picker>

        {/* <RNPickerSelect
          onValueChange={(value) => console.log(value)}
          items={[
            { label: 'Football', value: 'football' },
            { label: 'Baseball', value: 'baseball' },
            { label: 'Hockey', value: 'hockey' },
          ]}
        /> */}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar style='dark' />
      {isRunning ? (
        <Text style={styles.timerText}>{`${minutes}:${seconds}`}</Text>
      ) : (
        renderPickers()
      )}

      {!isRunning ? (
        <TouchableOpacity onPress={start} style={styles.button}>
          <Text style={styles.buttonText}>Start</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={stop}
          style={[styles.button, styles.buttonStop]}
        >
          <Text style={[styles.buttonText, styles.buttonTextStop]}>Stop</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#07121B',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    borderWidth: 10,
    borderColor: '#89AAFF',
    width: width / 2,
    height: width / 2,
    borderRadius: width / 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 70,
  },
  buttonText: {
    fontSize: 45,
    color: '#89AAFF',
  },
  timerText: {
    color: '#fff',
    fontSize: 90,
  },
  buttonStop: {
    borderColor: '#FF851B',
  },
  buttonTextStop: {
    color: '#FF851B',
  },
  picker: {
    width: 50,
    ...Platform.select({
      android: {
        color: '#fff',
        backgroundColor: '#07121B',
        marginLeft: 10,
      },
    }),
  },
  pickerItem: {
    color: '#fff',
    fontSize: 20,
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
