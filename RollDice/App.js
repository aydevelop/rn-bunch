import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

export default function App() {
  const dice = {
    1: require('./assets/dice_1.png'),
    2: require('./assets/dice_2.png'),
    3: require('./assets/dice_3.png'),
    4: require('./assets/dice_4.png'),
    5: require('./assets/dice_5.png'),
    6: require('./assets/dice_6.png'),
  };

  const [dice1, setDice1] = useState(dice['1']);
  const [dice2, setDice2] = useState(dice['2']);

  const roll = () => {
    let res1 = Math.floor(Math.random() * 6 + 1).toString();
    let res2 = Math.floor(Math.random() * 6 + 1).toString();

    setDice1(dice[res1]);
    setDice2(dice[res2]);
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <View style={{ flexDirection: 'row' }}>
        <Image style={{ width: 110, height: 110 }} source={dice1} />
        <Image
          style={{ width: 110, height: 110, marginLeft: 20 }}
          source={dice2}
        />
      </View>

      <TouchableOpacity onPress={roll}>
        <Text
          style={{
            backgroundColor: '#ff5733',
            paddingHorizontal: 40,
            paddingVertical: 10,
            marginTop: 40,
            borderRadius: 20,
          }}
        >
          ROLL THE DICE
        </Text>
      </TouchableOpacity>
    </View>
  );
}
