import * as React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { ceil } from 'react-native-reanimated';

const App = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          alert('Start');
        }}
      >
        <Text style={{ fontSize: 40, color: 'white' }}>
          Score: {props.route.params.score}
        </Text>
        <TouchableOpacity
          style={{ ...styles.button, marginTop: 30 }}
          onPress={() => {
            props.navigation.goBack();
          }}
        >
          <Text style={styles.buttonText}>Start New Quiz</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0033cc',
  },
  button: {
    borderWidth: 2,
    borderColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
  },
});

export default App;
