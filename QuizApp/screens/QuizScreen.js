import React, { useLayoutEffect, useState } from 'react';
import { Swing } from 'react-native-animated-spinkit';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AppLoading from 'expo-app-loading';
const entities = require('entities');

const amount = 5;
const apiURL = `https://opentdb.com/api.php?amount=${amount}&category=22&difficulty=easy&type=multiple`;

const Quiz = (props) => {
  const [score, setScore] = useState(0);
  const [values, setValues] = useState({
    currentQuestionIndex: 0,
    resultJSON: [],
    isLoaded: false,
  });
  const { currentQuestionIndex, resultJSON, isLoaded } = values;

  const checkAnswer = (selectedAnswer) => {
    console.log('------------------------');
    console.log(`selected ${selectedAnswer}`);
    console.log('------------------------');
    console.log(`correct ${resultJSON[currentQuestionIndex].correct_answer}`);
    console.log('------------------------');
    console.log(`score ${score}`);

    let index = currentQuestionIndex;
    index++;

    if (resultJSON[currentQuestionIndex].correct_answer == selectedAnswer) {
      setScore(score + 1);
    }

    if (index >= amount) {
      index = 0;
      setScore(1);

      props.navigation.navigate('Result', {
        score,
      });
    }

    setValues({ ...values, currentQuestionIndex: index });
  };

  const fetchQuestions = async () => {
    try {
      let json = await fetch(apiURL);
      let resultJSON = await json.json();
      setValues({ ...values, resultJSON: resultJSON.results, isLoaded: true });
    } catch (error) {
      console.error(error);
    }
  };

  const sort = (value) => {
    const answers = [...resultJSON[currentQuestionIndex].incorrect_answers];
    const correctAnswer = [...resultJSON[currentQuestionIndex].correct_answer];
    answers.push(correctAnswer);
    answers.sort(() => 0.5 > Math.random());

    return answers;
  };

  useLayoutEffect(() => {
    fetchQuestions();
  }, []);

  if (isLoaded) {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.questionContainer}>
          <Text style={styles.question}>
            {entities.decodeHTML(resultJSON[currentQuestionIndex].question)}
            {'  '}({currentQuestionIndex + 1}/{amount})
          </Text>
        </View>
        <View></View>
        <View>
          {sort(resultJSON[currentQuestionIndex]).map((a, index) => (
            <TouchableOpacity
              key={index + a}
              onPress={() => {
                checkAnswer(a);
              }}
              style={styles.button}
            >
              <Text>{a}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    );
  }
  return (
    <View style={styles.containerLoading}>
      <Swing size={60} color='#FFF' />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0033cc',
  },
  containerLoading: {
    flex: 1,
    backgroundColor: '#0033cc',
    justifyContent: 'center',
    alignItems: 'center',
  },
  questionContainer: {
    marginHorizontal: 30,
    marginVertical: 20,
    marginTop: 60,
  },
  question: {
    fontSize: 22,
    color: '#fff',
    textAlign: 'center',
  },
  answerContainer: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  button: {
    backgroundColor: '#D8D8D8',
    padding: 20,
    marginVertical: 10,
    borderRadius: 8,
    marginHorizontal: 20,
  },
  answerText: {
    fontSize: 18,
    color: '#fff',
  },
});

export default Quiz;
