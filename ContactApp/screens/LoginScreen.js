import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import firebaseApp from '../util/Firebase';

const LoginScreen = (props) => {
  const [loading, setLoading] = React.useState(false);
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    isLoggedIn: false,
  });
  const { email, password, isLoggedIn } = values;

  React.useEffect(() => {
    // firebaseApp.auth().onAuthStateChanged((result) => {
    //   if (result) {
    //     props.navigation.replace('Home');
    //   }
    // });

    const unsubscribe = props.navigation.addListener('focus', () => {
      const user = firebaseApp.auth().currentUser;
      if (user != null) {
        props.navigation.replace('Home');
      }
    });
    return unsubscribe;
  }, []);

  return (
    <View style={styles.container}>
      <Text
        style={{
          marginTop: 20,
          fontSize: 40,
          textAlign: 'center',
        }}
      >
        Sign In
      </Text>
      <View
        style={{
          marginTop: 20,
          marginHorizontal: 20,
        }}
      >
        <TextInput
          mode='outlined'
          label='Email'
          keyboardType='email-address'
          value={email}
          onChangeText={(email) => setValues({ ...values, email })}
        />
        <TextInput
          mode='outlined'
          style={{
            marginTop: 10,
          }}
          label='Password'
          keyboardType='visible-password'
          secureTextEntry={true}
          value={password}
          onChangeText={(password) => setValues({ ...values, password })}
        />
        <Button
          loading={loading}
          onPress={() => {
            if (email != '' && password != '') {
              setLoading(true);
              firebaseApp
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then((result) => {
                  const user = firebaseApp.auth().currentUser;
                  if (user != null) {
                    props.navigation.replace('Home');
                  } else {
                    alert('Provide information');
                  }
                })
                .catch((error) => {
                  alert(error);
                })
                .finally(() => {
                  setLoading(false);
                });
            } else {
              alert('Provide information');
            }
          }}
          mode='contained'
          style={{
            marginTop: 20,
          }}
        >
          Login Now
        </Button>
        <Button
          onPress={() => {
            props.navigation.navigate('Signup');
          }}
          style={{
            marginTop: 25,
          }}
        >
          New user? signup now
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default LoginScreen;
