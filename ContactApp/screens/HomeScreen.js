import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { Button, Icon, FAB, ActivityIndicator } from 'react-native-paper';
import firebaseApp from '../util/Firebase';
import { MaterialIcons } from '@expo/vector-icons';

const HomeScreen = (props) => {
  const [values, setValues] = React.useState({
    contacts: [],
    isLoaded: false,
  });
  const { contacts, isLoaded } = values;

  React.useEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        <Button
          icon='logout'
          onPress={() => {
            firebaseApp
              .auth()
              .signOut()
              .then(() => {
                props.navigation.replace('Login');
              })
              .catch((error) => console.log(error));
          }}
        >
          Logout
        </Button>
      ),
    });

    const unsubscribe = props.navigation.addListener('focus', () => {
      setValues({
        isLoaded: false,
        contacts: [],
      });
      getAllContacts();
    });

    return unsubscribe;
  }, []);

  const getAllContacts = () => {
    const user = firebaseApp.auth().currentUser;

    firebaseApp
      .firestore()
      .collection('data')
      .doc(user.uid)
      .collection('chat_contacts')
      .get()
      .then((querySnapshot) => {
        const contacts = [];
        querySnapshot.forEach((snapshot) => {
          contacts.push({
            id: snapshot.id,
            name: snapshot.data().name,
            phone: snapshot.data().phone,
          });
        });

        setValues({
          isLoaded: true,
          contacts,
        });
      })
      .catch((err) => {
        alert(err);
      });
  };

  if (!isLoaded) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={contacts}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Update', {
                  item: { ...item },
                });
              }}
            >
              <View style={styles.contact}>
                <View>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {item.name}
                  </Text>
                  <Text style={{ fontSize: 13 }}>{item.phone}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() => {
                      try {
                        const user = firebaseApp.auth().currentUser;
                        firebaseApp
                          .firestore()
                          .collection('data')
                          .doc(user.uid)
                          .collection('chat_contacts')
                          .doc(item.id)
                          .delete();

                        let tmp = [...contacts];
                        tmp = tmp.filter((c) => c.id !== item.id);
                        setValues({
                          isLoaded: true,
                          contacts: tmp,
                        });
                      } catch (err) {
                        alert(err);
                      }
                    }}
                  >
                    <MaterialIcons name='delete' size={32} color='purple' />
                  </TouchableOpacity>
                  <Text>{'   '}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(`tel:${item.phone}`);
                    }}
                  >
                    <MaterialIcons name='call' size={32} color='purple' />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
      <FAB
        icon='plus'
        style={styles.fab}
        onPress={() => {
          props.navigation.navigate('Add');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 20,
  },
  fab: {
    backgroundColor: 'purple',
    position: 'absolute',
    right: 0,
    bottom: 0,
    margin: 16,
  },
  contact: {
    backgroundColor: 'gray',
    marginTop: 15,
    marginHorizontal: 20,
    padding: 15,
    paddingHorizontal: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default HomeScreen;
