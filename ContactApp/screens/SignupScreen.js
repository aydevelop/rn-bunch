import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import firebaseApp from '../util/Firebase';

const LoginScreen = (props) => {
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    confirmPassword: '',
    isLoggedIn: false,
  });
  const { email, password, confirmPassword, isLoggedIn } = values;

  return (
    <View style={styles.container}>
      <Text
        style={{
          marginTop: 20,
          fontSize: 40,
          textAlign: 'center',
        }}
      >
        Sign Up
      </Text>
      <View
        style={{
          marginTop: 20,
          marginHorizontal: 20,
        }}
      >
        <TextInput
          mode='outlined'
          label='Email'
          keyboardType='email-address'
          value={email}
          onChangeText={(email) => setValues({ ...values, email })}
        />
        <TextInput
          mode='outlined'
          style={{
            marginTop: 10,
          }}
          label='Password'
          keyboardType='visible-password'
          secureTextEntry={true}
          value={password}
          onChangeText={(password) => setValues({ ...values, password })}
        />
        <TextInput
          mode='outlined'
          style={{
            marginTop: 10,
          }}
          label='Confirm Password'
          keyboardType='visible-password'
          secureTextEntry={true}
          value={confirmPassword}
          onChangeText={(confirmPassword) =>
            setValues({ ...values, confirmPassword })
          }
        />
        <Button
          onPress={() => {
            if (
              email != '' &&
              password != '' &&
              confirmPassword != '' &&
              password == confirmPassword
            ) {
              firebaseApp
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then((result) => {
                  console.log(result);
                  props.navigation.goBack();
                })
                .catch((error) => {
                  alert(error);
                });
            } else {
              alert('Provide correct details');
            }
          }}
          style={{
            marginTop: 25,
          }}
          mode='contained'
        >
          Signup now
        </Button>
        <Button
          onPress={() => {
            props.navigation.goBack();
          }}
          style={{
            marginTop: 20,
          }}
        >
          Exisiting user? signin now
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default LoginScreen;
