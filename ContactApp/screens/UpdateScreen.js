import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import firebaseApp from '../util/Firebase';

const UpdateScreen = (props) => {
  const [values, setValues] = React.useState({
    name: '',
    phone: '',
    id: '',
  });
  const { name, phone, id } = values;

  React.useEffect(() => {
    // props.navigation.setOptions({ title: 'Update contact' });

    let item = props.route.params.item;
    let name = item.name;
    let phone = item.phone;
    let id = item.id;

    setValues({ name, phone, id });
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={{
          marginTop: 20,
          marginHorizontal: 20,
        }}
      >
        <TextInput
          mode='outlined'
          label='Email'
          keyboardType='email-address'
          value={name}
          onChangeText={(name) => setValues({ ...values, name })}
        />
        <TextInput
          mode='outlined'
          style={{
            marginTop: 10,
          }}
          label='Password'
          keyboardType='visible-password'
          secureTextEntry={true}
          keyboardType='phone-pad'
          value={phone}
          onChangeText={(phone) => setValues({ ...values, phone })}
        />
        <Button
          onPress={() => {
            if (name != '' && phone != '') {
              const user = firebaseApp.auth().currentUser;

              firebaseApp
                .firestore()
                .collection('data')
                .doc(user.uid)
                .collection('chat_contacts')
                .doc(id)
                .update({
                  name,
                  phone,
                })
                .catch((err) => {
                  alert(err);
                })
                .then(() => {
                  props.navigation.goBack();
                });
            } else {
              alert('Provide information');
            }
          }}
          mode='contained'
          style={{
            marginTop: 30,
          }}
        >
          Update contact
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default UpdateScreen;
