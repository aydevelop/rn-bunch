import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import firebaseApp from '../util/Firebase';

const AddScreen = (props) => {
  const [values, setValues] = React.useState({
    name: '',
    phone: '',
  });
  const { name, phone } = values;

  React.useEffect(() => {
    // props.navigation.setOptions({ title: 'Add contact' });
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={{
          marginTop: 20,
          marginHorizontal: 20,
        }}
      >
        <TextInput
          mode='outlined'
          label='Name'
          value={name}
          onChangeText={(name) => setValues({ ...values, name })}
        />
        <TextInput
          mode='outlined'
          style={{
            marginTop: 10,
          }}
          label='Phone'
          keyboardType='visible-password'
          secureTextEntry={true}
          keyboardType='phone-pad'
          value={phone}
          onChangeText={(phone) => setValues({ ...values, phone })}
        />
        <Button
          onPress={() => {
            if (name != '' && phone != '') {
              const user = firebaseApp.auth().currentUser;

              firebaseApp
                .firestore()
                .collection('data')
                .doc(user.uid)
                .collection('chat_contacts')
                .add({
                  name,
                  phone,
                })
                .then((result) => {
                  props.navigation.goBack();
                })
                .catch((error) => {
                  alert(error);
                });
            } else {
              alert('Provide information');
            }
          }}
          mode='contained'
          style={{
            marginTop: 30,
          }}
        >
          Save contact
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default AddScreen;
