import firebase from 'firebase';

var firebaseConfig = {
  apiKey: 'AIzaSyATvTHrcrEDgc2N2kp2kVOZz_Yid7sO2yw',
  authDomain: 'test-af7de.firebaseapp.com',
  databaseURL: 'https://test-af7de.firebaseio.com',
  projectId: 'test-af7de',
  storageBucket: 'test-af7de.appspot.com',
  messagingSenderId: '1045243108173',
  appId: '1:1045243108173:web:9d65131723ffa075697504',
};

let firebaseApp;
if (!firebase.apps.length) {
  firebaseApp = firebase.initializeApp(firebaseConfig);
} else {
  firebaseApp = firebase.app();
}

export default firebaseApp;
