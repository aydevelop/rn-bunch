import React from 'react';

import { enableScreens } from 'react-native-screens';
enableScreens();
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

import AddScreen from './screens/AddScreen';
import UpdateScreen from './screens/UpdateScreen';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import SignupScreen from './screens/SignupScreen';

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Login'>
        <Stack.Screen name='Home' component={HomeScreen} />
        <Stack.Screen name='Login' component={LoginScreen} />
        <Stack.Screen name='Add' component={AddScreen} />
        <Stack.Screen name='Signup' component={SignupScreen} />
        <Stack.Screen name='Update' component={UpdateScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
